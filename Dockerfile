FROM openjdk:11-jdk
VOLUME /tmp
EXPOSE 8060
ADD target/dockerProj-0.0.1-SNAPSHOT.jar dockerProj-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/dockerProj-0.0.1-SNAPSHOT.jar"]

