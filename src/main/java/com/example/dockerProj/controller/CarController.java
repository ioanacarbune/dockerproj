package com.example.dockerProj.controller;

import com.example.dockerProj.model.Car;
import com.example.dockerProj.repo.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CarController {
    @Autowired
    private CarRepository carRepository;

    @GetMapping("/getCars")
    public List<Car> getFromDb2(){
        return carRepository.findAll();
    }

    @PostMapping("/save")
    public Car saveCars(@RequestBody Car c){
        return carRepository.save(c);
    }
}
