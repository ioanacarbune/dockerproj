package com.example.dockerProj.repo;

import com.example.dockerProj.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car,Integer> {
    Car findCarByName(String name);
}
