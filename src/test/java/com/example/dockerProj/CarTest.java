package com.example.dockerProj;

import com.example.dockerProj.model.Car;
import com.example.dockerProj.repo.CarRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;



public class CarTest {

    @Autowired
    private CarRepository repo;

    @BeforeEach
    public void before(){
        Car car=new Car("aaa");
        repo.save(car);
    }

    @Test
    public void testCreateUser(){
        Car car=repo.findCarByName("aaa");
        Assertions.assertEquals(car.getName(),"aaa");
    }

    @AfterEach
    public void after(){
        System.out.println("After testing..");
    }
}
